filetype on
call plug#begin('~/.vim/plugged') 
" I use the plugin manager from: https://github.com/junegunn/vim-plug
" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'jalvesaq/Nvim-R' " R stuff
Plug 'https://github.com/freeo/vim-kalisi' " colour scheme
Plug 'scrooloose/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'lervag/vimtex'
Plug 'ajh17/VimCompletesMe'

" Add plugins to &runtimepath
call plug#end()

" ----------------------------------
let maplocalleader = ","
let mapleader="\<SPACE>"

" -----------------------------------
"  Global stuff
map <F2> <Esc>:edit $MYVIMRC<CR>:source $MYVIMRC<CR> 
map <F3> <Esc>:NERDTreeToggle<CR>
map <F5> <Esc>:VimtexCompileSS<CR>:VimtexErrors<CR>

nnoremap <CR> i<CR><Esc>
nnoremap <Del> i<Del><Right><Esc>

inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}

inoremap (      ()<Left>
inoremap (<CR>  (<CR>)<Esc>O
inoremap ((     (
inoremap ()     ()

inoremap [      []<Left>
inoremap [<CR>  [<CR>]<Esc>O
inoremap [[     [
inoremap []     []

" ----------------------------------- 
"  Automation
" ----------------------------------- 

autocmd FileType tex setlocal formatoptions=tcn
autocmd FileType tex setlocal spell spelllang=en_gb
"autocmd FileType R setlocal formatoptions=tacn

" ----------------------------------- 
"  Autocomplete
" ----------------------------------- 

let omni_patterns =
            \ '\v\\%('
            \ . '\a*cite\a*%(\s*\[[^]]*\]){0,2}\s*\{[^}]*'
            \ . '|\a*ref%(\s*\{[^}]*|range\s*\{[^,}]*%(}\{)?)'
            \ . '|hyperref\s*\[[^]]*'
            \ . '|includegraphics\*?%(\s*\[[^]]*\]){0,2}\s*\{[^}]*'
            \ . '|%(include%(only)?|input)\s*\{[^}]*'
            \ . '|\a*(gls|Gls|GLS)(pl)?\a*%(\s*\[[^]]*\]){0,2}\s*\{[^}]*'
            \ . '|includepdf%(\s*\[[^]]*\])?\s*\{[^}]*'
            \ . '|includestandalone%(\s*\[[^]]*\])?\s*\{[^}]*'
            \ . ')'

augroup VimCompletesMeTex
    autocmd!
    autocmd FileType tex let b:vcm_omni_pattern = omni_patterns 
augroup END
set completeopt=longest,menuone


"  Navigation in windows
"  split:
nnoremap <Leader>s :split<CR> 
"  vertical split:
nnoremap <Leader>d <C-W>L 
"  close:
nnoremap <Leader>q :q<CR> 
" next window:
nnoremap <Leader>w <C-W>w 
" next buffer
nnoremap <Leader>e :bprevious<CR>
nnoremap <Leader>r :bnext<CR>

" ----------------------------------
" saving
" If the current buffer has never been saved, it will have no name,
" call the file browser to save it, otherwise just save it.
" When init is sourced in a runnning session delete Update first
command! -nargs=0 -bar Update if &modified 
	\|    if empty(bufname('%'))
	\|        browse confirm write
	\|    else
	\|        confirm write
	\|    endif
	\|endif
	
nnoremap <silent> <C-S> :<C-u>Update<CR>
inoremap <c-s> <Esc>:Update<CR>
vmap <C-s> <esc>:w<CR>gv


" -----------------------------------
"  Copy-Paste-Selection
" -----------------------------------

nnoremap <S-Up> v<Up>
nnoremap <S-Down> v<Down>
nnoremap <S-Left> v<Left>
nnoremap <S-Right> v<Right>
vnoremap <S-Up> <Up>
vnoremap <S-Down> <Down>
vnoremap <S-Left> <Left>
vnoremap <S-Right> <Right>
inoremap <S-Up> <Esc>v<Up>
inoremap <S-Down> <Esc>v<Down>
inoremap <S-Left> <Esc>v<Left>
inoremap <S-Right> <Esc>v<Right>

vnoremap <C-x> d<Esc>i
inoremap <C-v> <Esc>pi

"------------------------------------
" Nvim-R 
"------------------------------------

nnoremap <C-J> :call SendLineToR("down")<CR>
inoremap <C-J> <Esc>:call SendLineToR("down")<CR><Home>i
vnoremap <C-J> :call SendSelectionToR("echo", "stay")<CR><Esc>

" autofromat comments
autocmd FileType r setlocal formatoptions-=t formatoptions+=croql

let R_vsplit = 1         " vertical split by default
let R_ca_ck = 1          " to delete command in terminal before sending new
let R_openpdf = 1        " when using Rmd
let R_openhtml = 1       " when using Rmd
let R_show_args = 1      " argument names in omni complete
let R_args_in_stline = 1 " arguments in status line
let R_nvim_wd = 1        " working directory of R

" -----------------------------------
" vimlatex
" -----------------------------------

let g:vimtex_quickfix_ignored_warnings = [
            \ 'Underfull',
            \ 'Overfull',
            \ 'specifier changed to',
            \ ]

" -----------------------------------
" colour scheme

colorscheme kalisi
set background=dark 
set autoindent
set backup
set showcmd
set incsearch
set relativenumber
set number
set ruler
set splitright
set splitbelow
set nopaste
set linebreak
set wrap
set nolist
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set textwidth=80 

